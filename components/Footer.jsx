import React from 'react'
import Link from 'next/link'
import { AiFillInstagram, AiOutlineTwitter, AiFillFacebook, AiFillYoutube, AiOutlineCopyrightCircle, AiFillCopyrightCircle } from 'react-icons/ai'

const Footer = () => {
  return (
    <div className="footer-container">
      <small>&copy; 2022 Acoustics Lounge. All rights reserved</small>
      <p className="icons">
        <Link href="https://www.instagram.com/lazadaph/?hl=en" className="icon-link"><AiFillInstagram /></Link>
        <Link href="https://twitter.com/LazadaPH?" className="icon-link"><AiOutlineTwitter /></Link>
        <Link href="https://www.facebook.com/LazadaPhilippines/" className="icon-link"><AiFillFacebook /></Link>
        <Link href="https://www.youtube.com/channel/UCL6FFzNVSWXHC-kh66e7z-w" className="icon-link"><AiFillYoutube /></Link>
      </p>
    </div>
  )
}

export default Footer